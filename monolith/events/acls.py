import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
from django.http import JsonResponse

def get_photo(state):
    url = 'https://api.pexels.com/v1/search?query=nature&per_page=1'
    headers = {'Authorization': PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    photo_data = json.loads(response.content)
    # print("photo:", photo_data)
    photo_url = {
        "photo_url": photo_data["photos"][0]["url"]
    }
    # print(f"\n\n photo: {photo_url} \n\n")
    return photo_url

def get_weather_data(city,state):
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},840&appid={OPEN_WEATHER_API_KEY}"
    geo_response = requests.get(geo_url)
    geo_data = json.loads(geo_response.content)
    print(geo_data[0]["lat"])
    latitude= geo_data[0]["lat"]
    longitude = geo_data[0]["lon"]
    url = f'https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}'
    response = requests.get(url)
    weather_data = json.loads(response.content)
    print("weather: ", weather_data)
    weather = {
        "temp":weather_data["main"]["temp"],
        "weather": weather_data['weather'][0]['description']
    }
    return weather
